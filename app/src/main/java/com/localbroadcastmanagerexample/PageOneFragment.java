package com.localbroadcastmanagerexample;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class PageOneFragment extends Fragment implements View.OnClickListener {


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_page_one, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        Button button1 = view.findViewById(R.id.button1);
        Button button2 = view.findViewById(R.id.button2);
        Button button3 = view.findViewById(R.id.button3);
        Button button4 = view.findViewById(R.id.button4);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.button1) {
            sendLocalBroadCastOne();
        }else if(view.getId() == R.id.button2){
            sendGlobalBroadcast();
        }else if(view.getId() == R.id.button3){
            sendGlobalBroadcast2();
        }else if(view.getId() == R.id.button4){

            sendLocalBroadCastTwo();
        }
    }

    private void sendLocalBroadCastTwo() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.ACTION_EVENT_TWO);
        intent.putExtra("data","Event Two raise");

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        localBroadcastManager.sendBroadcast(intent);
    }

    private void sendLocalBroadCastOne() {
        Intent intent = new Intent();
        intent.setAction(MainActivity.ACTION_EVENT_ONE);
        intent.putExtra("data","Event ONE raise");

        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
        localBroadcastManager.sendBroadcast(intent);
    }


    private void sendGlobalBroadcast2() {
        Intent intent = new Intent();
        intent.setAction("CUSTOM_GLOBAL_EVENT_2");
        intent.putExtra("data","Send From LocalBroadcastManager example app");
        getContext().sendBroadcast(intent);
    }

    private void sendGlobalBroadcast() {
        Intent intent = new Intent();
        intent.setAction("CUSTOM_GLOBAL_EVENT");
        intent.putExtra("data","Send From LocalBroadcastManager example app 2");
        getContext().sendBroadcast(intent);
    }


}
