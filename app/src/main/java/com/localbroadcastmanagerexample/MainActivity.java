package com.localbroadcastmanagerexample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class MainActivity extends AppCompatActivity {

    public static final String ACTION_EVENT_ONE = "ACTION_EVENT_ONE";
    public static final String ACTION_EVENT_TWO = "ACTION_EVENT_TWO";
    private LocalBroadcastManager localBroadcastManager;
    private BroadcastReceiver broadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUI();
        registerLocalBroadcastReceiver();
    }

    private void registerLocalBroadcastReceiver() {
        localBroadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
        broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                if(intent.getAction().equalsIgnoreCase(ACTION_EVENT_TWO)){

                    Toast.makeText(getApplicationContext(),intent.getStringExtra("data"),Toast.LENGTH_LONG).show();
                }else if(intent.getAction().equalsIgnoreCase(ACTION_EVENT_ONE)){
                    Toast.makeText(getApplicationContext(),intent.getStringExtra("data"),Toast.LENGTH_LONG).show();

                }

            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_EVENT_TWO);
        intentFilter.addAction(ACTION_EVENT_ONE);

        localBroadcastManager.registerReceiver(broadcastReceiver,intentFilter);
    }


    private void initUI() {
        PageOneFragment pageOneFragment = new PageOneFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.content, pageOneFragment).commit();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
